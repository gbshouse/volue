﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Volue.Data
{
	public class DataService : IDataService
	{
		private readonly IConnectionMultiplexer _connection;

		public DataService(IConnectionMultiplexer connection)
		{
			_connection = connection;
		}

		public async Task AddTimeSeriesRange(IEnumerable<TimeSeriesModel> items)
		{
			var database = _connection.GetDatabase(0);

			var transaction = database.CreateTransaction();

			foreach (var subset in items.GroupBy(o => o.name))
			{
				await transaction.SetAddAsync(Common.NamesKey, subset.Key, CommandFlags.FireAndForget).ConfigureAwait(false);

				// todo: optimize inserts

				foreach (var item in subset)
				{
					await transaction.ExecuteAsync($"TS.ADD", new List<object>() { item.name, item.t, item.v }, CommandFlags.FireAndForget).ConfigureAwait(false);
				}
			}

			await transaction.ExecuteAsync(CommandFlags.FireAndForget).ConfigureAwait(false);
		}
	}

}
