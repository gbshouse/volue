﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Volue.Data
{
	public interface IDataService
	{
		Task AddTimeSeriesRange(IEnumerable<TimeSeriesModel> items);
	}
}