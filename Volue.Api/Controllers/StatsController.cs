﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volue.Calculation.Client;
using Volue.Data;

namespace Volue.Api.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class StatsController : ControllerBase
	{
		private readonly ICalculationClient _calculationClient;

		public StatsController(ICalculationClient calculationClient)
		{
			_calculationClient = calculationClient;
		}

		[HttpGet]
		public async Task<IActionResult> Get(long? from, long? to)
		{
			var result = await _calculationClient.CalculateStats(from, to).ConfigureAwait(false);

			return Ok(result);
		}
	}
}
