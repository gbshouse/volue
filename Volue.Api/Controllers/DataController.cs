﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volue.Calculation.Client;
using Volue.Data;

namespace Volue.Api.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class DataController : ControllerBase
	{
		private readonly IDataService _dataService;

		public DataController(IDataService dataService)
		{
			_dataService = dataService;
		}

		[HttpPost]
		public async Task<IActionResult> Post(IEnumerable<TimeSeriesModel> data)
		{
			await _dataService.AddTimeSeriesRange(data).ConfigureAwait(false);

			return CreatedAtAction(nameof(Post), null);
		}
	}
}
