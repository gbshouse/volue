﻿using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Volue.Api.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class StatusController : ControllerBase
	{
		private readonly IConnectionMultiplexer _connection;

		public StatusController(IConnectionMultiplexer connection)
		{
			_connection = connection;
		}

		[HttpGet]
		public IActionResult Get()
		{
			return Ok(_connection.GetStatus());
		}
	}
}
