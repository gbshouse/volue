﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Volue.Data
{
	[Serializable]
	public struct TimeSeriesStatsModel
	{
		public double avg { get; set; }
		public double sum { get; set; }
	}
}
