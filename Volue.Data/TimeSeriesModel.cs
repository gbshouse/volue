﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Volue.Data
{
	[Serializable]
	public struct TimeSeriesModel
	{
		public string name { get; set; }
		public long t { get; set; }
		public double v { get; set; }
	}
}
