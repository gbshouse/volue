﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using Volue.Data;

namespace Volue.Calculation.Client
{
	public class CalculationClient : ICalculationClient
	{
		private readonly IHttpClientFactory _clientFactory;

		private readonly string _baseUrl = "http://volue.calculation:5002/calculate";

		public CalculationClient(IHttpClientFactory clientFactory)
		{
			_clientFactory = clientFactory;
		}

		public async Task<TimeSeriesStatsModel?> CalculateStats(long? from, long? to)
		{
			using (var client = _clientFactory.CreateClient())
			{
				var response = await client.GetAsync(BuildUrl(from, to)).ConfigureAwait(false);

				if (response.IsSuccessStatusCode)
				{
					var payload = await response.Content.ReadAsStringAsync();
					return JsonSerializer.Deserialize<TimeSeriesStatsModel>(payload);
				}

				return null;
			}
		}

		private string BuildUrl(long? from, long? to)
		{
			return $"{_baseUrl}?from={from}&to={to}";
		}
	}
}
