﻿using System.Threading.Tasks;
using Volue.Data;

namespace Volue.Calculation.Client
{
	public interface ICalculationClient
	{
		Task<TimeSeriesStatsModel?> CalculateStats(long? from, long? to);
	}
}