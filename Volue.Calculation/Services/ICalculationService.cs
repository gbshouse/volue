﻿using System.Threading.Tasks;

namespace Volue.Data
{
	public interface ICalculationService
	{
		Task<TimeSeriesStatsModel> CalculateStats(long? min = null, long? max = null);
	}
}