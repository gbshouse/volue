﻿using StackExchange.Redis;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Volue.Data
{
	public class CalculationService : ICalculationService
	{
		private readonly IConnectionMultiplexer _connection;

		public CalculationService(IConnectionMultiplexer connection)
		{
			_connection = connection;
		}

		public async Task<TimeSeriesStatsModel> CalculateStats(long? min = null, long? max = null)
		{
			var database = _connection.GetDatabase(0);

			var minFilter = min.HasValue ? min.Value.ToString() : "-";
			var maxFilter = max.HasValue ? max.Value.ToString() : "+";

			double totalSum = 0;
			long totalCount = 0;

			foreach (var name in await database.SetMembersAsync(Common.NamesKey).ConfigureAwait(false))
			{
				if (await database.KeyExistsAsync((string)name).ConfigureAwait(false))
				{
					var sumResultRaw = await database.ExecuteAsync("TS.RANGE", name, minFilter, maxFilter, "AGGREGATION", "SUM", Int64.MaxValue).ConfigureAwait(false);

					if (TryParseResult(sumResultRaw, out RedisResult sumResult))
					{
						totalSum += (double)ParseResult(sumResult);
					}

					var countResultRaw = await database.ExecuteAsync("TS.RANGE", name, minFilter, maxFilter, "AGGREGATION", "COUNT", Int64.MaxValue).ConfigureAwait(false);

					if (TryParseResult(countResultRaw, out RedisResult countResult))
					{
						totalCount += (long)ParseResult(countResult);
					}
				}
			}

			return new TimeSeriesStatsModel()
			{
				sum = totalSum,
				avg = totalCount == 0 ? 0 : totalSum / totalCount
			};
		}

		private bool TryParseResult(RedisResult result, out RedisResult output)
		{
			output = ParseResult(result);
			return output != null;
		}

		private RedisResult ParseResult(RedisResult result)
		{
			if (result.Type == ResultType.MultiBulk)
			{
				var temp = (RedisResult[])result;

				if (temp.Length == 0)
				{
					return null;
				}
				else
				{
					return ParseResult(temp[temp.Length - 1]);
				}
			}
			else if (result.Type == ResultType.SimpleString)
			{
				return result;
			}

			return null;
		}
	}

}
