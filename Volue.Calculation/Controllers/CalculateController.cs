﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volue.Data;

namespace Volue.Calculation.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class CalculateController : ControllerBase
	{
		private readonly ICalculationService _service;

		public CalculateController(ICalculationService service)
		{
			_service = service;
		}

		[HttpGet]
		public async Task<IActionResult> Get(long? from = null, long? to = null)
		{
			var result = await _service.CalculateStats(from, to).ConfigureAwait(false);

			return Ok(result);
		}
	}
}
